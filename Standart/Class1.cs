﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace Standart
{
    public delegate void DocumentsReadyHandler(object obj, FileSystemEventArgs e);
    public delegate void TimeOutHandler();
    
    public class DocumentsReceiver
    {
        public event DocumentsReadyHandler DocumentsReady;
        public event TimeOutHandler TimeOut;

        private readonly Timer _timer;
        private readonly FileSystemWatcher _fileSystemWatcher;
        private Dictionary<string, bool> _files = new Dictionary<string, bool>();

        public bool AllFilesLoaded { get => _files.Values.All(v => v == true); } 

        public DocumentsReceiver( IEnumerable<string> listFilesWatching )
        {
            _timer = new Timer();
            _fileSystemWatcher = new FileSystemWatcher();
            listFilesWatching.ToList().ForEach(f => _files.Add(f, false));
        }

        public void Start(string targetDirectory, double waitingInterval)
        {
            _fileSystemWatcher.Path = targetDirectory;
            _fileSystemWatcher.EnableRaisingEvents = true;
            _fileSystemWatcher.Created += FileSystemWatcher_CreatedDeleted;
            _fileSystemWatcher.Deleted += FileSystemWatcher_CreatedDeleted;

            _timer.Interval = waitingInterval;
            _timer.Elapsed += _timer_Elapsed;
            _timer.Start();
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            TimeOut?.Invoke();
            UnsetSubscribtion();
        }

        private void FileSystemWatcher_CreatedDeleted(object sender, FileSystemEventArgs e)
        {
            if (CheckLoadedFiles(e))
            {
                DocumentsReady?.Invoke(this, e);
                if (AllFilesLoaded) 
                    UnsetSubscribtion();
            }
        }

        private bool CheckLoadedFiles(FileSystemEventArgs e)
        {
            if (_files.ContainsKey(e.Name))
            {
                _files[e.Name] = (e.ChangeType == WatcherChangeTypes.Created);
                return true;
            }
            return false;
        }

        private void UnsetSubscribtion()
        {
            _timer.Elapsed -= _timer_Elapsed;
            _fileSystemWatcher.Created -= FileSystemWatcher_CreatedDeleted;
            _fileSystemWatcher.Deleted -= FileSystemWatcher_CreatedDeleted;
        }

    }
}
