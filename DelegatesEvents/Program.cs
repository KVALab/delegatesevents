﻿using Microsoft.Extensions.Configuration;
using Standart;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DelegatesEvents
{
    class Program
    {        
        static void Main(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appconfig.json", optional: false, reloadOnChange: true)
                .Build();
            var progparam = configuration.GetSection("ProgramParams")
                .GetChildren()
                .ToDictionary(k=>k.Key, v=>v.Value);
            
            double waitingInterval;
            bool result = double.TryParse(progparam["WaitingInterval"], out waitingInterval);
            string folderPath = progparam["FolderPath"];

            try
            {   
                if (!result || !Directory.Exists(folderPath))
                    throw new ArgumentException("Конфигурационные данные приложения не корректны!");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            
            List<string> listFiles = new() { "Паспорт.jpg", "Заявление.txt", "Фото.jpg" };

            DocumentsReceiver reciver = new DocumentsReceiver( listFiles );
            reciver.DocumentsReady += DocumentsReceiver_DocumentsReady;
            reciver.TimeOut += DocumentsReceiver_TimeOut;
            reciver.Start(folderPath, waitingInterval);

            Console.ReadKey();
        }

        private static void DocumentsReceiver_TimeOut()
        {
            Console.WriteLine("Время для загрузки файлов вышло!");
        }

        private static void DocumentsReceiver_DocumentsReady(object obj, FileSystemEventArgs e)
        {
            switch (e.ChangeType)
            {
                case WatcherChangeTypes.Created:
                    Console.WriteLine($"Создан файл - {e.Name}");
                    break;
                case WatcherChangeTypes.Deleted:
                    Console.WriteLine($"Удален файл - {e.Name}");
                    break;                
            }

            if ( ((DocumentsReceiver)obj).AllFilesLoaded )
                Console.WriteLine("Все файлы успешно загружены!");
        }
    }
}
